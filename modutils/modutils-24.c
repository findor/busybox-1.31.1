/* vi: set sw=4 ts=4: */
/*
 * Mini insmod implementation for busybox
 *
 * This version of insmod supports ARM, CRIS, H8/300, x86, ia64, x86_64,
 * m68k, MIPS, PowerPC, S390, SH3/4/5, Sparc, v850e, and x86_64.
 *
 * Copyright (C) 1999-2004 by Erik Andersen <andersen@codepoet.org>
 * and Ron Alder <alder@lineo.com>
 *
 * Rodney Radford <rradford@mindspring.com> 17-Aug-2004.
 *   Added x86_64 support.
 *
 * Miles Bader <miles@gnu.org> added NEC V850E support.
 *
 * Modified by Bryan Rittmeyer <bryan@ixiacom.com> to support SH4
 * and (theoretically) SH3. I have only tested SH4 in little endian mode.
 *
 * Modified by Alcove, Julien Gaulmin <julien.gaulmin@alcove.fr> and
 * Nicolas Ferre <nicolas.ferre@alcove.fr> to support ARM7TDMI.  Only
 * very minor changes required to also work with StrongArm and presumably
 * all ARM based systems.
 *
 * Yoshinori Sato <ysato@users.sourceforge.jp> 19-May-2004.
 *   added Renesas H8/300 support.
 *
 * Paul Mundt <lethal@linux-sh.org> 08-Aug-2003.
 *   Integrated support for sh64 (SH-5), from preliminary modutils
 *   patches from Benedict Gaster <benedict.gaster@superh.com>.
 *   Currently limited to support for 32bit ABI.
 *
 * Magnus Damm <damm@opensource.se> 22-May-2002.
 *   The plt and got code are now using the same structs.
 *   Added generic linked list code to fully support PowerPC.
 *   Replaced the mess in arch_apply_relocation() with architecture blocks.
 *   The arch_create_got() function got cleaned up with architecture blocks.
 *   These blocks should be easy maintain and sync with obj_xxx.c in modutils.
 *
 * Magnus Damm <damm@opensource.se> added PowerPC support 20-Feb-2001.
 *   PowerPC specific code stolen from modutils-2.3.16,
 *   written by Paul Mackerras, Copyright 1996, 1997 Linux International.
 *   I've only tested the code on mpc8xx platforms in big-endian mode.
 *   Did some cleanup and added USE_xxx_ENTRIES...
 *
 * Quinn Jensen <jensenq@lineo.com> added MIPS support 23-Feb-2001.
 *   based on modutils-2.4.2
 *   MIPS specific support for Elf loading and relocation.
 *   Copyright 1996, 1997 Linux International.
 *   Contributed by Ralf Baechle <ralf@gnu.ai.mit.edu>
 *
 * Based almost entirely on the Linux modutils-2.3.11 implementation.
 *   Copyright 1996, 1997 Linux International.
 *   New implementation contributed by Richard Henderson <rth@tamu.edu>
 *   Based on original work by Bjorn Ekwall <bj0rn@blox.se>
 *   Restructured (and partly rewritten) by:
 *   BjÃ¶rn Ekwall <bj0rn@blox.se> February 1999
 *
 * Licensed under GPLv2 or later, see file LICENSE in this source tree.
 */

//kbuild:lib-$(CONFIG_FEATURE_2_4_MODULES) += modutils-24.o

#include "libbb.h"
#include "modutils.h"
#include <sys/utsname.h>

#if ENABLE_FEATURE_INSMOD_LOADINKMEM
#define LOADBITS 0
#else
#define LOADBITS 1
#endif

/* Alpha */
#if defined(__alpha__)
#define MATCH_MACHINE(x) (x == EM_ALPHA)
#define SHT_RELM       SHT_RELA
#define Elf64_RelM     Elf64_Rela
#define ELFCLASSM      ELFCLASS64
#endif

/* ARM support */
#if defined(__arm__)
#define MATCH_MACHINE(x) (x == EM_ARM)
#define SHT_RELM	SHT_REL
#define Elf32_RelM	Elf32_Rel
#define ELFCLASSM	ELFCLASS32
#define USE_PLT_ENTRIES
#define PLT_ENTRY_SIZE 8
#define USE_GOT_ENTRIES
#define GOT_ENTRY_SIZE 8
#define USE_SINGLE
#endif

/* NDS32 support */
#if defined(__nds32__) || defined(__NDS32__)
#define CONFIG_USE_GOT_ENTRIES
#define CONFIG_GOT_ENTRY_SIZE 4
#define CONFIG_USE_SINGLE

#if defined(__NDS32_EB__)
#define MATCH_MACHINE(x) (x == EM_NDS32)
#define SHT_RELM    SHT_RELA
#define Elf32_RelM  Elf32_Rela
#define ELFCLASSM   ELFCLASS32
#endif

#if defined(__NDS32_EL__)
#define MATCH_MACHINE(x) (x == EM_NDS32)
#define SHT_RELM    SHT_RELA
#define Elf32_RelM  Elf32_Rela
#define ELFCLASSM   ELFCLASS32
#endif
#endif

/* blackfin */
#if defined(BFIN)
#define MATCH_MACHINE(x) (x == EM_BLACKFIN)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#endif

/* CRIS */
#if defined(__cris__)
#define MATCH_MACHINE(x) (x == EM_CRIS)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#ifndef EM_CRIS
#define EM_CRIS 76
#define R_CRIS_NONE 0
#define R_CRIS_32   3
#endif
#endif

/* H8/300 */
#if defined(__H8300H__) || defined(__H8300S__)
#define MATCH_MACHINE(x) (x == EM_H8_300)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#define USE_SINGLE
#define SYMBOL_PREFIX	"_"
#endif

/* PA-RISC / HP-PA */
#if defined(__hppa__)
#define MATCH_MACHINE(x) (x == EM_PARISC)
#define SHT_RELM       SHT_RELA
#if defined(__LP64__)
#define Elf64_RelM     Elf64_Rela
#define ELFCLASSM      ELFCLASS64
#else
#define Elf32_RelM     Elf32_Rela
#define ELFCLASSM      ELFCLASS32
#endif
#endif

/* x86 */
#if defined(__i386__)
#ifndef EM_486
#define MATCH_MACHINE(x) (x == EM_386)
#else
#define MATCH_MACHINE(x) (x == EM_386 || x == EM_486)
#endif
#define SHT_RELM	SHT_REL
#define Elf32_RelM	Elf32_Rel
#define ELFCLASSM	ELFCLASS32
#define USE_GOT_ENTRIES
#define GOT_ENTRY_SIZE 4
#define USE_SINGLE
#endif

/* IA64, aka Itanium */
#if defined(__ia64__)
#define MATCH_MACHINE(x) (x == EM_IA_64)
#define SHT_RELM       SHT_RELA
#define Elf64_RelM     Elf64_Rela
#define ELFCLASSM      ELFCLASS64
#endif

/* m68k */
#if defined(__mc68000__)
#define MATCH_MACHINE(x) (x == EM_68K)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#define USE_GOT_ENTRIES
#define GOT_ENTRY_SIZE 4
#define USE_SINGLE
#endif

/* Microblaze */
#if defined(__microblaze__)
#define USE_SINGLE
#include <linux/elf-em.h>
#define MATCH_MACHINE(x) (x == EM_XILINX_MICROBLAZE)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#endif

/* MIPS */
#if defined(__mips__)
#define MATCH_MACHINE(x) (x == EM_MIPS || x == EM_MIPS_RS3_LE)
#define SHT_RELM	SHT_REL
#define Elf32_RelM	Elf32_Rel
#define ELFCLASSM	ELFCLASS32
/* Account for ELF spec changes.  */
#ifndef EM_MIPS_RS3_LE
#ifdef EM_MIPS_RS4_BE
#define EM_MIPS_RS3_LE	EM_MIPS_RS4_BE
#else
#define EM_MIPS_RS3_LE	10
#endif
#endif /* !EM_MIPS_RS3_LE */
#define ARCHDATAM       "__dbe_table"
#endif

/* Nios II */
#if defined(__nios2__)
#define MATCH_MACHINE(x) (x == EM_ALTERA_NIOS2)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#endif

/* PowerPC */
#if defined(__powerpc64__)
#define MATCH_MACHINE(x) (x == EM_PPC64)
#define SHT_RELM	SHT_RELA
#define Elf64_RelM	Elf64_Rela
#define ELFCLASSM	ELFCLASS64
#elif defined(__powerpc__)
#define MATCH_MACHINE(x) (x == EM_PPC)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#define USE_PLT_ENTRIES
#define PLT_ENTRY_SIZE 16
#define USE_PLT_LIST
#define LIST_ARCHTYPE ElfW(Addr)
#define USE_LIST
#define ARCHDATAM       "__ftr_fixup"
#endif

/* S390 */
#if defined(__s390__)
#define MATCH_MACHINE(x) (x == EM_S390)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#define USE_PLT_ENTRIES
#define PLT_ENTRY_SIZE 8
#define USE_GOT_ENTRIES
#define GOT_ENTRY_SIZE 8
#define USE_SINGLE
#endif

/* SuperH */
#if defined(__sh__)
#define MATCH_MACHINE(x) (x == EM_SH)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#define USE_GOT_ENTRIES
#define GOT_ENTRY_SIZE 4
#define USE_SINGLE
/* the SH changes have only been tested in =little endian= mode */
/* I'm not sure about big endian, so let's warn: */
#if defined(__sh__) && BB_BIG_ENDIAN
# error insmod.c may require changes for use on big endian SH
#endif
/* it may or may not work on the SH1/SH2... Error on those also */
#if ((!(defined(__SH3__) || defined(__SH4__) || defined(__SH5__)))) && (defined(__sh__))
#error insmod.c may require changes for SH1 or SH2 use
#endif
#endif

/* Sparc */
#if defined(__sparc__)
#define MATCH_MACHINE(x) (x == EM_SPARC)
#define SHT_RELM       SHT_RELA
#define Elf32_RelM     Elf32_Rela
#define ELFCLASSM      ELFCLASS32
#endif

/* v850e */
#if defined(__v850e__)
#define MATCH_MACHINE(x) ((x) == EM_V850 || (x) == EM_CYGNUS_V850)
#define SHT_RELM	SHT_RELA
#define Elf32_RelM	Elf32_Rela
#define ELFCLASSM	ELFCLASS32
#define USE_PLT_ENTRIES
#define PLT_ENTRY_SIZE 8
#define USE_SINGLE
#ifndef EM_CYGNUS_V850	/* grumble */
#define EM_CYGNUS_V850	0x9080
#endif
#define SYMBOL_PREFIX	"_"
#endif

/* X86_64  */
#if defined(__x86_64__)
#define MATCH_MACHINE(x) (x == EM_X86_64)
#define SHT_RELM	SHT_RELA
#define USE_GOT_ENTRIES
#define GOT_ENTRY_SIZE 8
#define USE_SINGLE
#define Elf64_RelM	Elf64_Rela
#define ELFCLASSM	ELFCLASS64
#endif

#ifndef SHT_RELM
#error Sorry, but insmod.c does not yet support this architecture...
#endif


//----------------------------------------------------------------------------
//--------modutils module.h, lines 45-242
//----------------------------------------------------------------------------

/* Definitions for the Linux module syscall interface.
   Copyright 1996, 1997 Linux International.

   Contributed by Richard Henderson <rth@tamu.edu>

   This file is part of the Linux modutils.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */


#ifndef MODUTILS_MODULE_H

/*======================================================================*/
/* For sizeof() which are related to the module platform and not to the
   environment isnmod is running in, use sizeof_xx instead of sizeof(xx).  */

#define tgt_sizeof_char		sizeof(char)
#define tgt_sizeof_short	sizeof(short)
#define tgt_sizeof_int		sizeof(int)
#define tgt_sizeof_long		sizeof(long)
#define tgt_sizeof_char_p	sizeof(char *)
#define tgt_sizeof_void_p	sizeof(void *)
#define tgt_long		long

#if defined(__sparc__) && !defined(__sparc_v9__) && defined(ARCH_sparc64)
#undef tgt_sizeof_long
#undef tgt_sizeof_char_p
#undef tgt_sizeof_void_p
#undef tgt_long
enum {
	tgt_sizeof_long = 8,
	tgt_sizeof_char_p = 8,
	tgt_sizeof_void_p = 8
};
#define tgt_long		long long
#endif

/*======================================================================*/
/* The structures used in Linux 2.1.  */

/* Note: new_module_symbol does not use tgt_long intentionally */
struct new_module_symbol {
	unsigned long value;
	unsigned long name;
};

struct new_module_persist;

struct new_module_ref {
	unsigned tgt_long dep;		/* kernel addresses */
	unsigned tgt_long ref;
	unsigned tgt_long next_ref;
};

struct new_module {
	unsigned tgt_long size_of_struct;	/* == sizeof(module) */
	unsigned tgt_long next;
	unsigned tgt_long name;
	unsigned tgt_long size;

	tgt_long usecount;
	unsigned tgt_long flags;		/* AUTOCLEAN et al */

	unsigned nsyms;
	unsigned ndeps;

	unsigned tgt_long syms;
	unsigned tgt_long deps;
	unsigned tgt_long refs;
	unsigned tgt_long init;
	unsigned tgt_long cleanup;
	unsigned tgt_long ex_table_start;
	unsigned tgt_long ex_table_end;
#ifdef __alpha__
	unsigned tgt_long gp;
#endif
	/* Everything after here is extension.  */
	unsigned tgt_long persist_start;
	unsigned tgt_long persist_end;
	unsigned tgt_long can_unload;
	unsigned tgt_long runsize;
	const char *kallsyms_start;     /* All symbols for kernel debugging */
	const char *kallsyms_end;
	const char *archdata_start;     /* arch specific data for module */
	const char *archdata_end;
	const char *kernel_data;        /* Reserved for kernel internal use */
};

#ifdef ARCHDATAM
#define ARCHDATA_SEC_NAME ARCHDATAM
#else
#define ARCHDATA_SEC_NAME "__archdata"
#endif
#define KALLSYMS_SEC_NAME "__kallsyms"


struct new_module_info {
	unsigned long addr;
	unsigned long size;
	unsigned long flags;
	long usecount;
};

/* Bits of module.flags.  */
enum {
	NEW_MOD_RUNNING = 1,
	NEW_MOD_DELETED = 2,
	NEW_MOD_AUTOCLEAN = 4,
	NEW_MOD_VISITED = 8,
	NEW_MOD_USED_ONCE = 16
};

int init_module(const char *name, const struct new_module *);
int query_module(const char *name, int which, void *buf,
		size_t bufsize, size_t *ret);

/* Values for query_module's which.  */
enum {
	QM_MODULES = 1,
	QM_DEPS = 2,
	QM_REFS = 3,
	QM_SYMBOLS = 4,
	QM_INFO = 5
};

/*======================================================================*/
/* The system calls unchanged between 2.0 and 2.1.  */

unsigned long create_module(const char *, size_t);
int delete_module(const char *module, unsigned int flags);


#endif /* module.h */

//----------------------------------------------------------------------------
//--------end of modutils module.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
//--------modutils obj.h, lines 253-462
//----------------------------------------------------------------------------

/* Elf object file loading and relocation routines.
   Copyright 1996, 1997 Linux International.

   Contributed by Richard Henderson <rth@tamu.edu>

   This file is part of the Linux modutils.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */


#ifndef MODUTILS_OBJ_H

/* The relocatable object is manipulated using elfin types.  */

#include <elf.h>
#include <endian.h>

#ifndef ElfW
# if ELFCLASSM == ELFCLASS32
#  define ElfW(x)  Elf32_ ## x
#  define ELFW(x)  ELF32_ ## x
# else
#  define ElfW(x)  Elf64_ ## x
#  define ELFW(x)  ELF64_ ## x
# endif
#endif

/* For some reason this is missing from some ancient C libraries....  */
#ifndef ELF32_ST_INFO
# define ELF32_ST_INFO(bind, type)       (((bind) << 4) + ((type) & 0xf))
#endif

#ifndef ELF64_ST_INFO
# define ELF64_ST_INFO(bind, type)       (((bind) << 4) + ((type) & 0xf))
#endif

#define ELF_ST_BIND(info) ELFW(ST_BIND)(info)
#define ELF_ST_TYPE(info) ELFW(ST_TYPE)(info)
#define ELF_ST_INFO(bind, type) ELFW(ST_INFO)(bind, type)
#define ELF_R_TYPE(val) ELFW(R_TYPE)(val)
#define ELF_R_SYM(val) ELFW(R_SYM)(val)

struct obj_string_patch;
struct obj_symbol_patch;

struct obj_section {
	ElfW(Shdr) header;
	const char *name;
	char *contents;
	struct obj_section *load_next;
	int idx;
};

struct obj_symbol {
	struct obj_symbol *next;	/* hash table link */
	const char *name;
	unsigned long value;
	unsigned long size;
	int secidx;			/* the defining section index/module */
	int info;
	int ksymidx;			/* for export to the kernel symtab */
	int referenced;		/* actually used in the link */
};

/* Hardcode the hash table size.  We shouldn't be needing so many
   symbols that we begin to degrade performance, and we get a big win
   by giving the compiler a constant divisor.  */

#define HASH_BUCKETS  521

struct obj_file {
	ElfW(Ehdr) header;
	ElfW(Addr) baseaddr;
	struct obj_section **sections;
	struct obj_section *load_order;
	struct obj_section **load_order_search_start;
	struct obj_string_patch *string_patches;
	struct obj_symbol_patch *symbol_patches;
	int (*symbol_cmp)(const char *, const char *); /* cant be FAST_FUNC */
	unsigned long (*symbol_hash)(const char *) FAST_FUNC;
	unsigned long local_symtab_size;
	struct obj_symbol **local_symtab;
	struct obj_symbol *symtab[HASH_BUCKETS];
};

enum obj_reloc {
	obj_reloc_ok,
	obj_reloc_overflow,
	obj_reloc_dangerous,
	obj_reloc_unhandled
};

struct obj_string_patch {
	struct obj_string_patch *next;
	int reloc_secidx;
	ElfW(Addr) reloc_offset;
	ElfW(Addr) string_offset;
};

struct obj_symbol_patch {
	struct obj_symbol_patch *next;
	int reloc_secidx;
	ElfW(Addr) reloc_offset;
	struct obj_symbol *sym;
};


/* Generic object manipulation routines.  */

static unsigned long FAST_FUNC obj_elf_hash(const char *);

static unsigned long obj_elf_hash_n(const char *, unsigned long len);

static struct obj_symbol *obj_find_symbol(struct obj_file *f,
		const char *name);

static ElfW(Addr) obj_symbol_final_value(struct obj_file *f,
		struct obj_symbol *sym);

#if ENABLE_FEATURE_INSMOD_VERSION_CHECKING
static void obj_set_symbol_compare(struct obj_file *f,
		int (*cmp)(const char *, const char *),
		unsigned long (*hash)(const char *) FAST_FUNC);
#endif

static struct obj_section *obj_find_section(struct obj_file *f,
		const char *name);

static void obj_insert_section_load_order(struct obj_file *f,
		struct obj_section *sec);

static struct obj_section *obj_create_alloced_section(struct obj_file *f,
		const char *name,
		unsigned long align,
		unsigned long size);

static struct obj_section *obj_create_alloced_section_first(struct obj_file *f,
		const char *name,
		unsigned long align,
		unsigned long size);

static void *obj_extend_section(struct obj_section *sec, unsigned long more);

static void obj_string_patch(struct obj_file *f, int secidx, ElfW(Addr) offset,
		const char *string);

static void obj_symbol_patch(struct obj_file *f, int secidx, ElfW(Addr) offset,
		struct obj_symbol *sym);

static void obj_check_undefineds(struct obj_file *f);

static void obj_allocate_commons(struct obj_file *f);

static unsigned long obj_load_size(struct obj_file *f);

static int obj_relocate(struct obj_file *f, ElfW(Addr) base);

#if !LOADBITS
#define obj_load(image, image_size, loadprogbits) \
	obj_load(image, image_size)
#endif
static struct obj_file *obj_load(char *image, size_t image_size, int loadprogbits);

static int obj_create_image(struct obj_file *f, char *image);

/* Architecture specific manipulation routines.  */

static struct obj_file *arch_new_file(void);

static struct obj_section *arch_new_section(void);

static struct obj_symbol *arch_new_symbol(void);

static enum obj_reloc arch_apply_relocation(struct obj_file *f,
		struct obj_section *targsec,
		/*struct obj_section *symsec,*/
		struct obj_symbol *sym,
		ElfW(RelM) *rel, ElfW(Addr) value);

static void arch_create_got(struct obj_file *f);
#if ENABLE_FEATURE_CHECK_TAINTED_MODULE
static int obj_gpl_license(struct obj_file *f, const char **license);
#endif
#endif /* obj.h */
//----------------------------------------------------------------------------
//--------end of modutils obj.h
//----------------------------------------------------------------------------


/* SPFX is always a string, so it can be concatenated to string constants.  */
#ifdef SYMBOL_PREFIX
#define SPFX	SYMBOL_PREFIX
#else
#define SPFX	""
#endif

enum { STRVERSIONLEN = 64 };

/*======================================================================*/

#define flag_force_load (option_mask32 & INSMOD_OPT_FORCE)
#define flag_autoclean (option_mask32 & INSMOD_OPT_KERNELD)
#define flag_verbose (option_mask32 & INSMOD_OPT_VERBOSE)
#define flag_quiet (option_mask32 & INSMOD_OPT_SILENT)
#define flag_noexport (option_mask32 & INSMOD_OPT_NO_EXPORT)
#define flag_print_load_map (option_mask32 & INSMOD_OPT_PRINT_MAP)

/*======================================================================*/

#if defined(USE_LIST)

struct arch_list_entry {
	struct arch_list_entry *next;
	LIST_ARCHTYPE addend;
	int offset;
	int inited : 1;
};

#endif

#if defined(USE_SINGLE)

struct arch_single_entry {
	int offset;
	int inited : 1;
	int allocated : 1;
};

#endif

#if defined(__mips__)
struct mips_hi16 {
	struct mips_hi16 *next;
	ElfW(Addr) *addr;
	ElfW(Addr) value;
};
#endif

struct arch_file {
	struct obj_file root;
#if defined(USE_PLT_ENTRIES)
	struct obj_section *plt;
#endif
#if defined(USE_GOT_ENTRIES)
	struct obj_section *got;
#endif
#if defined(__mips__)
	struct mips_hi16 *mips_hi16_list;
#endif
};

struct arch_symbol {
	struct obj_symbol root;
#if defined(USE_PLT_ENTRIES)
#if defined(USE_PLT_LIST)
	struct arch_list_entry *pltent;
#else
	struct arch_single_entry pltent;
#endif
#endif
#if defined(USE_GOT_ENTRIES)
	struct arch_single_entry gotent;
#endif
};


struct external_module {
	const char *name;
	ElfW(Addr) addr;
	int used;
	size_t nsyms;
	struct new_module_symbol *syms;
};

static struct new_module_symbol *ksyms;
static size_t nksyms;

static struct external_module *ext_modules;
static int n_ext_modules;
static int n_ext_modules_used;

/*======================================================================*/


static struct obj_file *arch_new_file(void)
{
	struct arch_file *f;
	f = xzalloc(sizeof(*f));
	return &f->root; /* it's a first member */
}

static struct obj_section *arch_new_section(void)
{
	return xzalloc(sizeof(struct obj_section));
}

static struct obj_symbol *arch_new_symbol(void)
{
	struct arch_symbol *sym;
	sym = xzalloc(sizeof(*sym));
	return &sym->root;
}

static enum obj_reloc
arch_apply_relocation(struct obj_file *f,
		struct obj_section *targsec,
		/*struct obj_section *symsec,*/
		struct obj_symbol *sym,
		ElfW(RelM) *rel, ElfW(Addr) v)
{
#if defined(__arm__) || defined(__i386__) || defined(__mc68000__) \
 || defined(__sh__) || defined(__s390__) || defined(__x86_64__) \
 || defined(__powerpc__) || defined(__mips__)
	struct arch_file *ifile = (struct arch_file *) f;
#endif
	enum obj_reloc ret = obj_reloc_ok;
	ElfW(Addr) *loc = (ElfW(Addr) *) (targsec->contents + rel->r_offset);
#if defined(__arm__) || defined(__H8300H__) || defined(__H8300S__) \
 || defined(__i386__) || defined(__mc68000__) || defined(__microblaze__) \
 || defined(__mips__) || defined(__nios2__) || defined(__powerpc__) \
 || defined(__s390__) || defined(__sh__) || defined(__x86_64__)
	ElfW(Addr) dot = targsec->header.sh_addr + rel->r_offset;
#endif
#if defined(USE_GOT_ENTRIES) || defined(USE_PLT_ENTRIES)
	struct arch_symbol *isym = (struct arch_symbol *) sym;
#endif
#if defined(__arm__) || defined(__i386__) || defined(__mc68000__) \
 || defined(__sh__) || defined(__s390__)
#if defined(USE_GOT_ENTRIES)
	ElfW(Addr) got = ifile->got ? ifile->got->header.sh_addr : 0;
#endif
#endif
#if defined(USE_PLT_ENTRIES)
	ElfW(Addr) plt = ifile->plt ? ifile->plt->header.sh_addr : 0;
	unsigned long *ip;
# if defined(USE_PLT_LIST)
	struct arch_list_entry *pe;
# else
	struct arch_single_entry *pe;
# endif
#endif

	switch (ELF_R_TYPE(rel->r_info)) {

#if defined(__arm__)

		case R_ARM_NONE:
			break;

		case R_ARM_ABS32:
			*loc += v;
			break;

		case R_ARM_GOT32:
			goto bb_use_got;

		case R_ARM_GOTPC:
			/* relative reloc, always to _GLOBAL_OFFSET_TABLE_
			 * (which is .got) similar to branch,
			 * but is full 32 bits relative */

			*loc += got - dot;
			break;

		case R_ARM_PC24:
		case R_ARM_PLT32:
			goto bb_use_plt;

		case R_ARM_GOTOFF: /* address relative to the got */
			*loc += v - got;
			break;

#elif defined(__cris__)

		case R_CRIS_NONE:
			break;

		case R_CRIS_32:
			/* CRIS keeps the relocation value in the r_addend field and
			 * should not use whats in *loc at all
			 */
			*loc = v;
			break;

#elif defined(__H8300H__) || defined(__H8300S__)

		case R_H8_DIR24R8:
			loc = (ElfW(Addr) *)((ElfW(Addr))loc - 1);
			*loc = (*loc & 0xff000000) | ((*loc & 0xffffff) + v);
			break;
		case R_H8_DIR24A8:
			*loc += v;
			break;
		case R_H8_DIR32:
		case R_H8_DIR32A16:
			*loc += v;
			break;
		case R_H8_PCREL16:
			v -= dot + 2;
			if ((ElfW(Sword))v > 0x7fff
			 || (ElfW(Sword))v < -(ElfW(Sword))0x8000
			) {
				ret = obj_reloc_overflow;
			} else {
				*(unsigned short *)loc = v;
			}
			break;
		case R_H8_PCREL8:
			v -= dot + 1;
			if ((ElfW(Sword))v > 0x7f
			 || (ElfW(Sword))v < -(ElfW(Sword))0x80
			) {
				ret = obj_reloc_overflow;
			} else {
				*(unsigned char *)loc = v;
			}
			break;

#elif defined(__i386__)

		case R_386_NONE:
			break;

		case R_386_32:
			*loc += v;
			break;

		case R_386_PLT32:
		case R_386_PC32:
		case R_386_GOTOFF:
			*loc += v - dot;
			break;

		case R_386_GLOB_DAT:
		case R_386_JMP_SLOT:
			*loc = v;
			break;

		case R_386_RELATIVE:
			*loc += f->baseaddr;
			break;

		case R_386_GOTPC:
			*loc += got - dot;
			break;

		case R_386_GOT32:
			goto bb_use_got;
			break;

#elif defined(__microblaze__)
		case R_MICROBLAZE_NONE:
		case R_MICROBLAZE_64_NONE:
		case R_MICROBLAZE_32_SYM_OP_SYM:
		case R_MICROBLAZE_32_PCREL:
			break;

		case R_MICROBLAZE_64_PCREL: {
			/* dot is the address of the current instruction.
			 * v is the target symbol address.
			 * So we need to extract the offset in the code,
			 * adding v, then subtrating the current address
			 * of this instruction.
			 * Ex: "IMM 0xFFFE  bralid 0x0000" = "bralid 0xFFFE0000"
			 */

			/* Get split offset stored in code */
			unsigned int temp = (loc[0] & 0xFFFF) << 16 |
						(loc[1] & 0xFFFF);

			/* Adjust relative offset. -4 adjustment required
			 * because dot points to the IMM insn, but branch
			 * is computed relative to the branch instruction itself.
			 */
			temp += v - dot - 4;

			/* Store back into code */
			loc[0] = (loc[0] & 0xFFFF0000) | temp >> 16;
			loc[1] = (loc[1] & 0xFFFF0000) | (temp & 0xFFFF);

			break;
		}

		case R_MICROBLAZE_32:
			*loc += v;
			break;

		case R_MICROBLAZE_64: {
			/* Get split pointer stored in code */
			unsigned int temp1 = (loc[0] & 0xFFFF) << 16 |
						(loc[1] & 0xFFFF);

			/* Add reloc offset */
			temp1+=v;

			/* Store back into code */
			loc[0] = (loc[0] & 0xFFFF0000) | temp1 >> 16;
			loc[1] = (loc[1] & 0xFFFF0000) | (temp1 & 0xFFFF);

			break;
		}

		case R_MICROBLAZE_32_PCREL_LO:
		case R_MICROBLAZE_32_LO:
		case R_MICROBLAZE_SRO32:
		case R_MICROBLAZE_SRW32:
			ret = obj_reloc_unhandled;
			break;

#elif defined(__mc68000__)

		case R_68K_NONE:
			break;

		case R_68K_32:
			*loc += v;
			break;

		case R_68K_8:
			if (v > 0xff) {
				ret = obj_reloc_overflow;
			}
			*(char *)loc = v;
			break;

		case R_68K_16:
			if (v > 0xffff) {
				ret = obj_reloc_overflow;
			}
			*(short *)loc = v;
			break;

		case R_68K_PC8:
			v -= dot;
			if ((ElfW(Sword))v > 0x7f
			 || (ElfW(Sword))v < -(ElfW(Sword))0x80
			) {
				ret = obj_reloc_overflow;
			}
			*(char *)loc = v;
			break;

		case R_68K_PC16:
			v -= dot;
			if ((ElfW(Sword))v > 0x7fff
			 || (ElfW(Sword))v < -(ElfW(Sword))0x8000
			) {
				ret = obj_reloc_overflow;
			}
			*(short *)loc = v;
			break;

		case R_68K_PC32:
			*(int *)loc = v - dot;
			break;

		case R_68K_GLOB_DAT:
		case R_68K_JMP_SLOT:
			*loc = v;
			break;

		case R_68K_RELATIVE:
			*(int *)loc += f->baseaddr;
			break;

		case R_68K_GOT32:
			goto bb_use_got;

# ifdef R_68K_GOTOFF
		case R_68K_GOTOFF:
			*loc += v - got;
			break;
# endif

#elif defined(__mips__)

		case R_MIPS_NONE:
			break;

		case R_MIPS_32:
			*loc += v;
			break;

		case R_MIPS_26:
			if (v % 4)
				ret = obj_reloc_dangerous;
			if ((v & 0xf0000000) != ((dot + 4) & 0xf0000000))
				ret = obj_reloc_overflow;
			*loc =
				(*loc & ~0x03ffffff) | ((*loc + (v >> 2)) &
										0x03ffffff);
			break;

		case R_MIPS_HI16:
			{
				struct mips_hi16 *n;

				/* We cannot relocate this one now because we don't know the value
				   of the carry we need to add.  Save the information, and let LO16
				   do the actual relocation.  */
				n = xmalloc(sizeof *n);
				n->addr = loc;
				n->value = v;
				n->next = ifile->mips_hi16_list;
				ifile->mips_hi16_list = n;
				break;
			}

		case R_MIPS_LO16:
			{
				unsigned long insnlo = *loc;
				ElfW(Addr) val, vallo;

				/* Sign extend the addend we extract from the lo insn.  */
				vallo = ((insnlo & 0xffff) ^ 0x8000) - 0x8000;

				if (ifile->mips_hi16_list != NULL) {
					struct mips_hi16 *l;

					l = ifile->mips_hi16_list;
					while (l != NULL) {
						struct mips_hi16 *next;
						unsigned long insn;

						/* Do the HI16 relocation.  Note that we actually don't
						   need to know anything about the LO16 itself, except where
						   to find the low 16 bits of the addend needed by the LO16.  */
						insn = *l->addr;
						val =
							((insn & 0xffff) << 16) +
							vallo;
						val += v;

						/* Account for the sign extension that will happen in the
						   low bits.  */
						val =
							((val >> 16) +
							 ((val & 0x8000) !=
							  0)) & 0xffff;

						insn = (insn & ~0xffff) | val;
						*l->addr = insn;

						next = l->next;
						free(l);
						l = next;
					}

					ifile->mips_hi16_list = NULL;
				}

				/* Ok, we're done with the HI16 relocs.  Now deal with the LO16.  */
				val = v + vallo;
				insnlo = (insnlo & ~0xffff) | (val & 0xffff);
				*loc = insnlo;
				break;
			}

#elif defined(__nios2__)

		case R_NIOS2_NONE:
			break;

		case R_NIOS2_BFD_RELOC_32:
			*loc += v;
			break;

		case R_NIOS2_BFD_RELOC_16:
			if (v > 0xffff) {
				ret = obj_reloc_overflow;
			}
			*(short *)loc = v;
			break;

		case R_NIOS2_BFD_RELOC_8:
			if (v > 0xff) {
				ret = obj_reloc_overflow;
			}
			*(char *)loc = v;
			break;

		case R_NIOS2_S16:
			{
				Elf32_Addr word;

				if ((Elf32_Sword)v > 0x7fff
				 || (Elf32_Sword)v < -(Elf32_Sword)0x8000
				) {
					ret = obj_reloc_overflow;
				}

				word = *loc;
				*loc = ((((word >> 22) << 16) | (v & 0xffff)) << 6) |
				       (word & 0x3f);
			}
			break;

		case R_NIOS2_U16:
			{
				Elf32_Addr word;

				if (v > 0xffff) {
					ret = obj_reloc_overflow;
				}

				word = *loc;
				*loc = ((((word >> 22) << 16) | (v & 0xffff)) << 6) |
				       (word & 0x3f);
			}
			break;

		case R_NIOS2_PCREL16:
			{
				Elf32_Addr word;

				v -= dot + 4;
				if ((Elf32_Sword)v > 0x7fff
				 || (Elf32_Sword)v < -(Elf32_Sword)0x8000
				) {
					ret = obj_reloc_overflow;
				}

				word = *loc;
				*loc = ((((word >> 22) << 16) | (v & 0xffff)) << 6) | (word & 0x3f);
			}
			break;

		case R_NIOS2_GPREL:
			{
				Elf32_Addr word, gp;
				/* get _gp */
				gp = obj_symbol_final_value(f, obj_find_symbol(f, SPFX "_gp"));
				v -= gp;
				if ((Elf32_Sword)v > 0x7fff
				 || (Elf32_Sword)v < -(Elf32_Sword)0x8000
				) {
					ret = obj_reloc_overflow;
				}

				word = *loc;
				*loc = ((((word >> 22) << 16) | (v & 0xffff)) << 6) | (word & 0x3f);
			}
			break;

		case R_NIOS2_CALL26:
			if (v & 3)
				ret = obj_reloc_dangerous;
			if ((v >> 28) != (dot >> 28))
				ret = obj_reloc_overflow;
			*loc = (*loc & 0x3f) | ((v >> 2) << 6);
			break;

		case R_NIOS2_IMM5:
			{
				Elf32_Addr word;

				if (v > 0x1f) {
					ret = obj_reloc_overflow;
				}

				word = *loc & ~0x7c0;
				*loc = word | ((v & 0x1f) << 6);
			}
			break;

		case R_NIOS2_IMM6:
			{
				Elf32_Addr word;

				if (v > 0x3f) {
					ret = obj_reloc_overflow;
				}

				word = *loc & ~0xfc0;
				*loc = word | ((v & 0x3f) << 6);
			}
			break;

		case R_NIOS2_IMM8:
			{
				Elf32_Addr word;

				if (v > 0xff) {
					ret = obj_reloc_overflow;
				}

				word = *loc & ~0x3fc0;
				*loc = word | ((v & 0xff) << 6);
			}
			break;

		case R_NIOS2_HI16:
			{
				Elf32_Addr word;

				word = *loc;
				*loc = ((((word >> 22) << 16) | ((v >>16) & 0xffff)) << 6) |
				       (word & 0x3f);
			}
			break;

		case R_NIOS2_LO16:
			{
				Elf32_Addr word;

				word = *loc;
				*loc = ((((word >> 22) << 16) | (v & 0xffff)) << 6) |
				       (word & 0x3f);
			}
			break;

		case R_NIOS2_HIADJ16:
			{
				Elf32_Addr word1, word2;

				word1 = *loc;
				word2 = ((v >> 16) + ((v >> 15) & 1)) & 0xffff;
				*loc = ((((word1 >> 22) << 16) | word2) << 6) |
				       (word1 & 0x3f);
			}
			break;

#elif defined(__powerpc64__)
		/* PPC64 needs a 2.6 kernel, 2.4 module relocation irrelevant */

#elif defined(__powerpc__)

		case R_PPC_ADDR16_HA:
			*(unsigned short *)loc = (v + 0x8000) >> 16;
			break;

		case R_PPC_ADDR16_HI:
			*(unsigned short *)loc = v >> 16;
			break;

		case R_PPC_ADDR16_LO:
			*(unsigned short *)loc = v;
			break;

		case R_PPC_REL24:
			goto bb_use_plt;

		case R_PPC_REL32:
			*loc = v - dot;
			break;

		case R_PPC_ADDR32:
			*loc = v;
			break;

#elif defined(__s390__)

		case R_390_32:
			*(unsigned int *) loc += v;
			break;
		case R_390_16:
			*(unsigned short *) loc += v;
			break;
		case R_390_8:
			*(unsigned char *) loc += v;
			break;

		case R_390_PC32:
			*(unsigned int *) loc += v - dot;
			break;
		case R_390_PC16DBL:
			*(unsigned short *) loc += (v - dot) >> 1;
			break;
		case R_390_PC16:
			*(unsigned shoror