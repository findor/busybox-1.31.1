/* This is a generated file, don't edit */

#define NUM_APPLETS 233
#define KNOWN_APPNAME_OFFSETS 8

const uint16_t applet_nameofs[] ALIGN2 = {
159,
346,
546,
722,
922,
1128,
1305,
};

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"addgroup" "\0"
"ar" "\0"
"arch" "\0"
"ash" "\0"
"awk" "\0"
"basename" "\0"
"bc" "\0"
"blkdiscard" "\0"
"bunzip2" "\0"
"bzcat" "\0"
"bzip2" "\0"
"cal" "\0"
"cat" "\0"
"chattr" "\0"
"chgrp" "\0"
"chmod" "\0"
"chown" "\0"
"chroot" "\0"
"chrt" "\0"
"cksum" "\0"
"clear" "\0"
"cmp" "\0"
"comm" "\0"
"cp" "\0"
"cpio" "\0"
"crontab" "\0"
"cut" "\0"
"dc" "\0"
"dd" "\0"
"delgroup" "\0"
"diff" "\0"
"dirname" "\0"
"dnsd" "\0"
"dnsdomainname" "\0"
"dos2unix" "\0"
"du" "\0"
"echo" "\0"
"ed" "\0"
"egrep" "\0"
"env" "\0"
"expand" "\0"
"expr" "\0"
"factor" "\0"
"fakeidentd" "\0"
"fallocate" "\0"
"false" "\0"
"fatattr" "\0"
"fgrep" "\0"
"find" "\0"
"flock" "\0"
"fold" "\0"
"fsfreeze" "\0"
"fstrim" "\0"
"fsync" "\0"
"ftpd" "\0"
"ftpget" "\0"
"ftpput" "\0"
"getopt" "\0"
"grep" "\0"
"groups" "\0"
"gunzip" "\0"
"gzip" "\0"
"hd" "\0"
"head" "\0"
"hexdump" "\0"
"hexedit" "\0"
"hostid" "\0"
"hostname" "\0"
"httpd" "\0"
"i2cdetect" "\0"
"i2cdump" "\0"
"i2cget" "\0"
"i2cset" "\0"
"i2ctransfer" "\0"
"id" "\0"
"ifdown" "\0"
"ifup" "\0"
"install" "\0"
"iostat" "\0"
"ipcalc" "\0"
"ipneigh" "\0"
"kill" "\0"
"killall" "\0"
"killall5" "\0"
"less" "\0"
"link" "\0"
"linux32" "\0"
"linux64" "\0"
"ln" "\0"
"logger" "\0"
"logname" "\0"
"logread" "\0"
"lpq" "\0"
"lpr" "\0"
"ls" "\0"
"lsof" "\0"
"lspci" "\0"
"lsscsi" "\0"
"lsusb" "\0"
"lzcat" "\0"
"lzma" "\0"
"lzop" "\0"
"md5sum" "\0"
"microcom" "\0"
"mkdir" "\0"
"mkdosfs" "\0"
"mke2fs" "\0"
"mkfifo" "\0"
"mkpasswd" "\0"
"mktemp" "\0"
"more" "\0"
"mpstat" "\0"
"mv" "\0"
"nc" "\0"
"nice" "\0"
"nl" "\0"
"nohup" "\0"
"nologin" "\0"
"nproc" "\0"
"nsenter" "\0"
"nuke" "\0"
"od" "\0"
"partprobe" "\0"
"paste" "\0"
"patch" "\0"
"pgrep" "\0"
"pipe_progress" "\0"
"pkill" "\0"
"pmap" "\0"
"poweroff" "\0"
"powertop" "\0"
"printenv" "\0"
"printf" "\0"
"ps" "\0"
"pscan" "\0"
"pstree" "\0"
"pwd" "\0"
"pwdx" "\0"
"readlink" "\0"
"readprofile" "\0"
"realpath" "\0"
"reboot" "\0"
"renice" "\0"
"reset" "\0"
"resize" "\0"
"resume" "\0"
"rev" "\0"
"rm" "\0"
"rmdir" "\0"
"rpm" "\0"
"rpm2cpio" "\0"
"run-init" "\0"
"run-parts" "\0"
"scriptreplay" "\0"
"sed" "\0"
"seq" "\0"
"setfattr" "\0"
"setpriv" "\0"
"setserial" "\0"
"setsid" "\0"
"sh" "\0"
"sha1sum" "\0"
"sha256sum" "\0"
"sha3sum" "\0"
"sha512sum" "\0"
"shred" "\0"
"shuf" "\0"
"sleep" "\0"
"smemcap" "\0"
"sort" "\0"
"split" "\0"
"ssl_client" "\0"
"strings" "\0"
"sum" "\0"
"svc" "\0"
"svok" "\0"
"swapoff" "\0"
"swapon" "\0"
"sync" "\0"
"sysctl" "\0"
"syslogd" "\0"
"tail" "\0"
"tar" "\0"
"tc" "\0"
"tee" "\0"
"telnet" "\0"
"telnetd" "\0"
"test" "\0"
"tftp" "\0"
"tftpd" "\0"
"timeout" "\0"
"touch" "\0"
"tr" "\0"
"true" "\0"
"truncate" "\0"
"ts" "\0"
"tty" "\0"
"ttysize" "\0"
"ubimkvol" "\0"
"ubirename" "\0"
"ubirmvol" "\0"
"ubirsvol" "\0"
"ubiupdatevol" "\0"
"uevent" "\0"
"uname" "\0"
"uncompress" "\0"
"unexpand" "\0"
"uniq" "\0"
"unix2dos" "\0"
"unlink" "\0"
"unlzma" "\0"
"unshare" "\0"
"unxz" "\0"
"unzip" "\0"
"usleep" "\0"
"uudecode" "\0"
"uuencode" "\0"
"vi" "\0"
"volname" "\0"
"watch" "\0"
"wc" "\0"
"wget" "\0"
"which" "\0"
"whoami" "\0"
"whois" "\0"
"xargs" "\0"
"xxd" "\0"
"xz" "\0"
"xzcat" "\0"
"yes" "\0"
"zcat" "\0"
;

#define APPLET_NO_addgroup 2
#define APPLET_NO_ar 3
#define APPLET_NO_arch 4
#define APPLET_NO_ash 5
#define APPLET_NO_awk 6
#define APPLET_NO_basename 7
#define APPLET_NO_bc 8
#define APPLET_NO_blkdiscard 9
#define APPLET_NO_bunzip2 10
#define APPLET_NO_bzcat 11
#define APPLET_NO_bzip2 12
#define APPLET_NO_cal 13
#define APPLET_NO_cat 14
#define APPLET_NO_chattr 15
#define APPLET_NO_chgrp 16
#define APPLET_NO_chmod 17
#define APPLET_NO_chown 18
#define APPLET_NO_chroot 19
#define APPLET_NO_chrt 20
#define APPLET_NO_cksum 21
#define APPLET_NO_clear 22
#define APPLET_NO_cmp 23
#define APPLET_NO_comm 24
#define APPLET_NO_cp 25
#define APPLET_NO_cpio 26
#define APPLET_NO_crontab 27
#define APPLET_NO_cut 28
#define APPLET_NO_dc 29
#define APPLET_NO_dd 30
#define APPLET_NO_delgroup 31
#define APPLET_NO_diff 32
#define APPLET_NO_dirname 33
#define APPLET_NO_dnsd 34
#define APPLET_NO_dnsdomainname 35
#define APPLET_NO_dos2unix 36
#define APPLET_NO_du 37
#define APPLET_NO_echo 38
#define APPLET_NO_ed 39
#define APPLET_NO_egrep 40
#define APPLET_NO_env 41
#define APPLET_NO_expand 42
#define APPLET_NO_expr 43
#define APPLET_NO_factor 44
#define APPLET_NO_fakeidentd 45
#define APPLET_NO_fallocate 46
#define APPLET_NO_false 47
#define APPLET_NO_fatattr 48
#define APPLET_NO_fgrep 49
#define APPLET_NO_find 50
#define APPLET_NO_flock 51
#define APPLET_NO_fold 52
#define APPLET_NO_fsfreeze 53
#define APPLET_NO_fstrim 54
#define APPLET_NO_fsync 55
#define APPLET_NO_ftpd 56
#define APPLET_NO_ftpget 57
#define APPLET_NO_ftpput 58
#define APPLET_NO_getopt 59
#define APPLET_NO_grep 60
#define APPLET_NO_groups 61
#define APPLET_NO_gunzip 62
#define APPLET_NO_gzip 63
#define APPLET_NO_hd 64
#define APPLET_NO_head 65
#define APPLET_NO_hexdump 66
#define APPLET_NO_hexedit 67
#define APPLET_NO_hostid 68
#define APPLET_NO_hostname 69
#define APPLET_NO_httpd 70
#define APPLET_NO_i2cdetect 71
#define APPLET_NO_i2cdump 72
#define APPLET_NO_i2cget 73
#define APPLET_NO_i2cset 74
#define APPLET_NO_i2ctransfer 75
#define APPLET_NO_id 76
#define APPLET_NO_ifdown 77
#define APPLET_NO_ifup 78
#define APPLET_NO_install 79
#define APPLET_NO_iostat 80
#define APPLET_NO_ipcalc 81
#define APPLET_NO_ipneigh 82
#define APPLET_NO_kill 83
#define APPLET_NO_killall 84
#define APPLET_NO_killall5 85
#define APPLET_NO_less 86
#define APPLET_NO_link 87
#define APPLET_NO_linux32 88
#define APPLET_NO_linux64 89
#define APPLET_NO_ln 90
#define APPLET_NO_logger 91
#define APPLET_NO_logname 92
#define APPLET_NO_logread 93
#define APPLET_NO_lpq 94
#define APPLET_NO_lpr 95
#define APPLET_NO_ls 96
#define APPLET_NO_lsof 97
#define APPLET_NO_lspci 98
#define APPLET_NO_lsscsi 99
#define APPLET_NO_lsusb 100
#define APPLET_NO_lzcat 101
#define APPLET_NO_lzma 102
#define APPLET_NO_lzop 103
#define APPLET_NO_md5sum 104
#define APPLET_NO_microcom 105
#define APPLET_NO_mkdir 106
#define APPLET_NO_mkdosfs 107
#define APPLET_NO_mke2fs 108
#define APPLET_NO_mkfifo 109
#define APPLET_NO_mkpasswd 110
#define APPLET_NO_mktemp 111
#define APPLET_NO_more 112
#define APPLET_NO_mpstat 113
#define APPLET_NO_mv 114
#define APPLET_NO_nc 115
#define APPLET_NO_nice 116
#define APPLET_NO_nl 117
#define APPLET_NO_nohup 118
#define APPLET_NO_nologin 119
#define APPLET_NO_nproc 120
#define APPLET_NO_nsenter 121
#define APPLET_NO_nuke 122
#define APPLET_NO_od 123
#define APPLET_NO_partprobe 124
#define APPLET_NO_paste 125
#define APPLET_NO_patch 126
#define APPLET_NO_pgrep 127
#define APPLET_NO_pipe_progress 128
#define APPLET_NO_pkill 129
#define APPLET_NO_pmap 130
#define APPLET_NO_poweroff 131
#define APPLET_NO_powertop 132
#define APPLET_NO_printenv 133
#define APPLET_NO_printf 134
#define APPLET_NO_ps 135
#define APPLET_NO_pscan 136
#define APPLET_NO_pstree 137
#define APPLET_NO_pwd 138
#define APPLET_NO_pwdx 139
#define APPLET_NO_readlink 140
#define APPLET_NO_readprofile 141
#define APPLET_NO_realpath 142
#define APPLET_NO_reboot 143
#define APPLET_NO_renice 144
#define APPLET_NO_reset 145
#define APPLET_NO_resize 146
#define APPLET_NO_resume 147
#define APPLET_NO_rev 148
#define APPLET_NO_rm 149
#define APPLET_NO_rmdir 150
#define APPLET_NO_rpm 151
#define APPLET_NO_rpm2cpio 152
#define APPLET_NO_scriptreplay 155
#define APPLET_NO_sed 156
#define APPLET_NO_seq 157
#define APPLET_NO_setfattr 158
#define APPLET_NO_setpriv 159
#define APPLET_NO_setserial 160
#define APPLET_NO_setsid 161
#define APPLET_NO_sh 162
#define APPLET_NO_sha1sum 163
#define APPLET_NO_sha256sum 164
#define APPLET_NO_sha3sum 165
#define APPLET_NO_sha512sum 166
#define APPLET_NO_shred 167
#define APPLET_NO_shuf 168
#define APPLET_NO_sleep 169
#define APPLET_NO_smemcap 170
#define APPLET_NO_sort 171
#define APPLET_NO_split 172
#define APPLET_NO_ssl_client 173
#define APPLET_NO_strings 174
#define APPLET_NO_sum 175
#define APPLET_NO_svc 176
#define APPLET_NO_svok 177
#define APPLET_NO_swapoff 178
#define APPLET_NO_swapon 179
#define APPLET_NO_sync 180
#define APPLET_NO_sysctl 181
#define APPLET_NO_syslogd 182
#define APPLET_NO_tail 183
#define APPLET_NO_tar 184
#define APPLET_NO_tc 185
#define APPLET_NO_tee 186
#define APPLET_NO_telnet 187
#define APPLET_NO_telnetd 188
#define APPLET_NO_test 189
#define APPLET_NO_tftp 190
#define APPLET_NO_tftpd 191
#define APPLET_NO_timeout 192
#define APPLET_NO_touch 193
#define APPLET_NO_tr 194
#define APPLET_NO_true 195
#define APPLET_NO_truncate 196
#define APPLET_NO_ts 197
#define APPLET_NO_tty 198
#define APPLET_NO_ttysize 199
#define APPLET_NO_ubimkvol 200
#define APPLET_NO_ubirename 201
#define APPLET_NO_ubirmvol 202
#define APPLET_NO_ubirsvol 203
#define APPLET_NO_ubiupdatevol 204
#define APPLET_NO_uevent 205
#define APPLET_NO_uname 206
#define APPLET_NO_uncompress 207
#define APPLET_NO_unexpand 208
#define APPLET_NO_uniq 209
#define APPLET_NO_unix2dos 210
#define APPLET_NO_unlink 211
#define APPLET_NO_unlzma 212
#define APPLET_NO_unshare 213
#define APPLET_NO_unxz 214
#define APPLET_NO_unzip 215
#define APPLET_NO_usleep 216
#define APPLET_NO_uudecode 217
#define APPLET_NO_uuencode 218
#define APPLET_NO_vi 219
#define APPLET_NO_volname 220
#define APPLET_NO_watch 221
#define APPLET_NO_wc 222
#define APPLET_NO_wget 223
#define APPLET_NO_which 224
#define APPLET_NO_whoami 225
#define APPLET_NO_whois 226
#define APPLET_NO_xargs 227
#define APPLET_NO_xxd 228
#define APPLET_NO_xz 229
#define APPLET_NO_xzcat 230
#define APPLET_NO_yes 231
#define APPLET_NO_zcat 232

#ifndef SKIP_applet_main
int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
addgroup_main,
ar_main,
uname_main,
ash_main,
awk_main,
basename_main,
bc_main,
blkdiscard_main,
bunzip2_main,
bunzip2_main,
bzip2_main,
cal_main,
cat_main,
chattr_main,
chgrp_main,
chmod_main,
chown_main,
chroot_main,
chrt_main,
cksum_main,
clear_main,
cmp_main,
comm_main,
cp_main,
cpio_main,
crontab_main,
cut_main,
dc_main,
dd_main,
deluser_main,
diff_main,
dirname_main,
dnsd_main,
hostname_main,
dos2unix_main,
du_main,
echo_main,
ed_main,
grep_main,
env_main,
expand_main,
expr_main,
factor_main,
fakeidentd_main,
fallocate_main,
false_main,
fatattr_main,
grep_main,
find_main,
flock_main,
fold_main,
fsfreeze_main,
fstrim_main,
fsync_main,
ftpd_main,
ftpgetput_main,
ftpgetput_main,
getopt_main,
grep_main,
id_main,
gunzip_main,
gzip_main,
hexdump_main,
head_main,
hexdump_main,
hexedit_main,
hostid_main,
hostname_main,
httpd_main,
i2cdetect_main,
i2cdump_main,
i2cget_main,
i2cset_main,
i2ctransfer_main,
id_main,
ifupdown_main,
ifupdown_main,
install_main,
iostat_main,
ipcalc_main,
ipneigh_main,
kill_main,
kill_main,
kill_main,
less_main,
link_main,
setarch_main,
setarch_main,
ln_main,
logger_main,
logname_main,
logread_main,
lpqr_main,
lpqr_main,
ls_main,
lsof_main,
lspci_main,
lsscsi_main,
lsusb_main,
unlzma_main,
unlzma_main,
lzop_main,
md5_sha1_sum_main,
microcom_main,
mkdir_main,
mkfs_vfat_main,
mkfs_ext2_main,
mkfifo_main,
cryptpw_main,
mktemp_main,
more_main,
mpstat_main,
mv_main,
nc_main,
nice_main,
nl_main,
nohup_main,
scripted_main,
nproc_main,
nsenter_main,
nuke_main,
od_main,
partprobe_main,
paste_main,
patch_main,
pgrep_main,
pipe_progress_main,
pgrep_main,
pmap_main,
halt_main,
powertop_main,
printenv_main,
printf_main,
ps_main,
pscan_main,
pstree_main,
pwd_main,
pwdx_main,
readlink_main,
readprofile_main,
realpath_main,
halt_main,
renice_main,
reset_main,
resize_main,
resume_main,
rev_main,
rm_main,
rmdir_main,
rpm_main,
rpm2cpio_main,
switch_root_main,
run_parts_main,
scriptreplay_main,
sed_main,
seq_main,
setfattr_main,
setpriv_main,
setserial_main,
setsid_main,
ash_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
md5_sha1_sum_main,
shred_main,
shuf_main,
sleep_main,
smemcap_main,
sort_main,
split_main,
ssl_client_main,
strings_main,
sum_main,
svc_main,
svok_main,
swap_on_off_main,
swap_on_off_main,
sync_main,
sysctl_main,
syslogd_main,
tail_main,
tar_main,
tc_main,
tee_main,
telnet_main,
telnetd_main,
test_main,
tftp_main,
tftpd_main,
timeout_main,
touch_main,
tr_main,
true_main,
truncate_main,
ts_main,
tty_main,
ttysize_main,
ubi_tools_main,
ubirename_main,
ubi_tools_main,
ubi_tools_main,
ubi_tools_main,
uevent_main,
uname_main,
uncompress_main,
expand_main,
uniq_main,
dos2unix_main,
unlink_main,
unlzma_main,
unshare_main,
unxz_main,
unzip_main,
usleep_main,
uudecode_main,
uuencode_main,
vi_main,
volname_main,
watch_main,
wc_main,
wget_main,
which_main,
whoami_main,
whois_main,
xargs_main,
xxd_main,
unxz_main,
unxz_main,
yes_main,
gunzip_main,
};
#endif

const uint8_t applet_suid[] ALIGN1 = {
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x80,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
0x00,
};

const uint8_t applet_install_loc[] ALIGN1 = {
0x33,
0x34,
0x11,
0x33,
0x33,
0x33,
0x33,
0x11,
0x11,
0x41,
0x33,
0x33,
0x13,
0x31,
0x33,
0x41,
0x33,
0x14,
0x33,
0x11,
0x31,
0x33,
0x43,
0x13,
0x11,
0x33,
0x43,
0x12,
0x34,
0x13,
0x31,
0x11,
0x33,
0x33,
0x13,
0x44,
0x44,
0x44,
0x23,
0x32,
0x11,
0x12,
0x43,
0x13,
0x11,
0x31,
0x23,
0x33,
0x31,
0x33,
0x33,
0x13,
0x33,
0x21,
0x32,
0x13,
0x11,
0x31,
0x31,
0x43,
0x33,
0x31,
0x34,
0x33,
0x31,
0x23,
0x14,
0x13,
0x33,
0x31,
0x43,
0x23,
0x33,
0x13,
0x11,
0x11,
0x23,
0x11,
0x31,
0x13,
0x31,
0x31,
0x33,
0x33,
0x13,
0x33,
0x33,
0x33,
0x33,
0x22,
0x21,
0x32,
0x21,
0x33,
0x34,
0x43,
0x13,
0x13,
0x33,
0x33,
0x44,
0x44,
0x24,
0x11,
0x33,
0x33,
0x33,
0x33,
0x31,
0x13,
0x13,
0x33,
0x33,
0x33,
0x33,
0x33,
0x01,
};
